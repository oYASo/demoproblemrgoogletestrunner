#ifndef _APP_H_
#define _APP_H_

#ifdef GoogleTestRunner_EXPORTS
# define APP_EXPORT _declspec(dllexport)
#else
# define APP_EXPORT _declspec(dllimport)
#endif

namespace App
{
	APP_EXPORT int add(int a, int b);
} // App

#endif // _APP_H_
